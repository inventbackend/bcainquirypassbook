package model;

import com.google.gson.annotations.SerializedName;

public class mdlAccountStatus {
    @SerializedName("open_indicator")
    public String openIndicator;
    @SerializedName("post_indicator")
    public String postIndicator;
    @SerializedName("dormant_status")
    public String dormantStatus;
    @SerializedName("miscellaneous_status")
    public String miscellaneousStatus;
}
