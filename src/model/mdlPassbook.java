package model;

import com.google.gson.annotations.SerializedName;

public class mdlPassbook {
    @SerializedName("account_number")
    public String accountNumber;
    @SerializedName("account_name")
    public String accountName;
    @SerializedName("account_type")
    public String accountType;
    @SerializedName("currency_code")
    public String currencyCode;
    @SerializedName("branch_code")
    public String branchCode;
    @SerializedName("passbook_detail")
    public mdlPassbookDetail passbookDetail;
    @SerializedName("open_date")
    public String openDate;
    @SerializedName("deposit_category")
    public String depositCategory;
    @SerializedName("account_status")
    public mdlAccountStatus accountStatus;
    @SerializedName("ledger_balance")
    public String ledgerBalance;
    @SerializedName("current_balance")
    public String currentBalance;
    @SerializedName("available_balance")
    public String availableBalance;
    @SerializedName("close_balance")
    public String closeBalance;
    @SerializedName("statement_cycle")
    public String statementCycle;
    @SerializedName("interest_cycle")
    public String interestCycle;
    @SerializedName("overdraft_change_cycle")
    public String overdraftChangeCycle;
    @SerializedName("analysis_cycle")
    public String analysisCycle;
    @SerializedName("overdraft_code")
    public String overdraftCode;
    @SerializedName("overdraft_limit")
    public String overdraftLimit;
    @SerializedName("overdraft_plan")
    public String overdraftPlan;
    @SerializedName("overdraft_interest_account")
    public String overdraftInterestAccount;
    @SerializedName("online_live_amount")
    public String onlineLiveAmount;
    @SerializedName("online_memo")
    public String onlineMemo;
    @SerializedName("float_entered_online")
    public String floatEnteredOnline;
    @SerializedName("online_float_yesterday")
    public String onlineFloatYesterday;
    @SerializedName("online_float")
    public String onlineFloat;
    @SerializedName("online_float_tomorrow")
    public String onlineFloatTomorrow;
    @SerializedName("stop_indicator")
    public String stopIndicator;
    @SerializedName("hold_status")
    public String holdStatus;
    @SerializedName("pra_indicator")
    public String praIndicator;
    @SerializedName("collection_disbursment_indicator")
    public String collectionDisbursmentIndicator;
    @SerializedName("deferred_income_indicator")
    public String deferredIncomeIndicator;
    @SerializedName("combine_statement_indicator")
    public String combineStatementIndicator;
    @SerializedName("user_code")
    public String userCode;
    @SerializedName("saving_transfer_code")
    public String savingTransferCode;
    @SerializedName("total_amount")
    public String totalAmount;
    @SerializedName("float_amount")
    public String floatAmount;
    @SerializedName("accrued_amount")
    public String accruedAmount;
    @SerializedName("activity")
    public String activity;
    @SerializedName("time_deposit_level")
    public String timeDepositLevel;
    @SerializedName("last_active")
    public String lastActive;
    @SerializedName("penalty")
    public String penalty;
    @SerializedName("interest_paid_year_to_date")
    public String interestPaidYearToDate;
    @SerializedName("accrued_interest")
    public String accruedInterest;
    @SerializedName("tax_close")
    public String taxClose;
    @SerializedName("cost_center")
    public String costCenter;
    @SerializedName("message")
    public String message;
    @SerializedName("officers")
    public String officers;
}
