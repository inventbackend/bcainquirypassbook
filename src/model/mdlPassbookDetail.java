package model;

import com.google.gson.annotations.SerializedName;

public class mdlPassbookDetail {
    @SerializedName("page_number")
    public String pageNumber;
    @SerializedName("line_number")
    public String lineNumber;
    @SerializedName("serial_number")
    public String serialNumber;
    @SerializedName("passbook_balance")
    public String passbookBalance;
    @SerializedName("passbook_indicator")
    public String passbookIndicator;
    @SerializedName("unprinted_transaction")
    public String unprintedTransaction;
}
