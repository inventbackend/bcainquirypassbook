package com.bca.controller;

import java.text.MessageFormat;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;

import adapter.PassbookAdapter;
import adapter.LogAdapter;

@RestController
public class controller {
    final static Logger logger = LogManager.getLogger(controller.class);

    @RequestMapping(value = "/ping", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody String GetPing() {
	String ConnectionStatus = "true";
	return ConnectionStatus;
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{account_number}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String GetPassbookDetails(@PathVariable("account_number") String accountNumber,
	    @RequestParam(value = "serial", defaultValue = "") String serialNumber, @RequestParam(value = "wsid", defaultValue = "") String wsid,
	    HttpServletResponse response) {
	long startTime = System.currentTimeMillis();
	model.mdlAPIResult mdlInquiryPassbookResult = new model.mdlAPIResult();
	model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
	model.mdlMessage mdlMessage = new model.mdlMessage();

	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.WSID = wsid;
	mdlLog.SerialNumber = serialNumber;
	mdlLog.ApiFunction = "GetPassbookDetails";
	mdlLog.SystemFunction = "GetPassbookDetails";
	mdlLog.LogSource = "Webservice";
	mdlLog.LogStatus = "Failed";
	boolean isValid = true;
	Gson gson = new Gson();
	Object[] params = new Object[] { accountNumber, serialNumber, wsid };
	String customDataTemplate = "AccountNumber: {0}, SerialNumber: {1}, WSID: {2}";
	String customData = MessageFormat.format(customDataTemplate, params);
	String jsonOut = "";

	try {
	    if (accountNumber == null || accountNumber.equals("")) {
		mdlErrorSchema.ErrorCode = "01";
		mdlMessage.Indonesian = "Nomor rekening tidak valid";
		mdlMessage.English = mdlLog.ErrorMessage = "Account number is not valid";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlInquiryPassbookResult.ErrorSchema = mdlErrorSchema;
		LogAdapter.InsertLog(mdlLog);
		jsonOut = gson.toJson(mdlInquiryPassbookResult);
		logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, "/inquiry-passbook", "GET", customData, "", jsonOut));
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		return gson.toJson(mdlInquiryPassbookResult);
	    }

	    model.mdlPassbookResult mdlPassbookResult = PassbookAdapter.GetPassbookData(accountNumber, serialNumber, wsid);

	    if (mdlPassbookResult == null || mdlPassbookResult.ErrorSchema == null) {
		mdlErrorSchema.ErrorCode = "02";
		mdlMessage.Indonesian = "Gagal memanggil service GetPassbookDetails";
		mdlMessage.English = mdlLog.ErrorMessage = "Service GetPassbookDetails call failed";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlInquiryPassbookResult.ErrorSchema = mdlErrorSchema;
		isValid = false;
	    } else if (!mdlPassbookResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		mdlErrorSchema.ErrorCode = mdlPassbookResult.ErrorSchema.ErrorCode;
		mdlMessage.Indonesian = mdlPassbookResult.ErrorSchema.ErrorMessage.Indonesian;
		mdlMessage.English = mdlLog.ErrorMessage = mdlPassbookResult.ErrorSchema.ErrorMessage.English;
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlInquiryPassbookResult.ErrorSchema = mdlErrorSchema;
		isValid = false;
	    }

	    if (!isValid) {
		LogAdapter.InsertLog(mdlLog);
		jsonOut = gson.toJson(mdlInquiryPassbookResult);
		logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, "/inquiry-passbook", "GET", customData + ", mdlPassbookResult: "
			+ gson.toJson(mdlPassbookResult), "", jsonOut));
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		return gson.toJson(mdlInquiryPassbookResult);
	    }

	    if (!mdlPassbookResult.OutputSchema.passbookDetail.serialNumber.equalsIgnoreCase("")) {
		model.mdlAPIResult mdlCheckSmallPassbookResult = new model.mdlAPIResult();
		mdlCheckSmallPassbookResult = PassbookAdapter.GetSmallPassbookStatement(accountNumber, serialNumber, wsid);
		isValid = true;
		if (mdlCheckSmallPassbookResult == null || mdlCheckSmallPassbookResult.ErrorSchema == null) {
		    mdlErrorSchema.ErrorCode = "03";
		    mdlMessage.Indonesian = "Gagal memanggil service GetSmallPassbookStatement";
		    mdlMessage.English = mdlLog.ErrorMessage = "Service GetSmallPassbookStatement call failed";
		    mdlErrorSchema.ErrorMessage = mdlMessage;
		    mdlInquiryPassbookResult.ErrorSchema = mdlErrorSchema;
		    isValid = false;
		} else if (!mdlCheckSmallPassbookResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		    mdlErrorSchema.ErrorCode = mdlCheckSmallPassbookResult.ErrorSchema.ErrorCode;
		    mdlMessage.Indonesian = mdlCheckSmallPassbookResult.ErrorSchema.ErrorMessage.Indonesian;
		    mdlMessage.English = mdlLog.ErrorMessage = mdlCheckSmallPassbookResult.ErrorSchema.ErrorMessage.English;
		    mdlErrorSchema.ErrorMessage = mdlMessage;
		    mdlInquiryPassbookResult.ErrorSchema = mdlErrorSchema;
		    isValid = false;
		}
		if (!isValid) {
		    LogAdapter.InsertLog(mdlLog);
		    jsonOut = gson.toJson(mdlInquiryPassbookResult);
		    logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, "/inquiry-passbook", "GET", customData + ", mdlPassbookResult: "
			    + gson.toJson(mdlPassbookResult), "", jsonOut));
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		    return gson.toJson(mdlInquiryPassbookResult);
		}

		LinkedTreeMap<String, JsonObject> outputSchemaMap = (LinkedTreeMap<String, JsonObject>) mdlCheckSmallPassbookResult.OutputSchema;
		JsonObject smallPassbookObject = gson.toJsonTree(outputSchemaMap).getAsJsonObject();
		String haveTransaction = smallPassbookObject.get("flag").getAsString();
		if (haveTransaction.equalsIgnoreCase("Y")) {
		    mdlPassbookResult.OutputSchema.passbookDetail.unprintedTransaction = "1";
		} else {
		    mdlPassbookResult.OutputSchema.passbookDetail.unprintedTransaction = "0";
		}
	    }

	    mdlErrorSchema.ErrorCode = "00";
	    mdlMessage.Indonesian = "Berhasil";
	    mdlMessage.English = mdlLog.LogStatus = "Success";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    mdlInquiryPassbookResult.ErrorSchema = mdlErrorSchema;
	    mdlInquiryPassbookResult.OutputSchema = mdlPassbookResult.OutputSchema;
	    jsonOut = gson.toJson(mdlInquiryPassbookResult);
	    logger.info(LogAdapter.logControllerToLog4j(true, startTime, 200, "/inquiry-passbook", "GET", customData, "", jsonOut));
	} catch (Exception ex) {
	    mdlErrorSchema.ErrorCode = "04";
	    mdlMessage.Indonesian = "Gagal memanggil service";
	    mdlMessage.English = mdlLog.ErrorMessage = "Service call failed";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    mdlInquiryPassbookResult.ErrorSchema = mdlErrorSchema;
	    mdlLog.ErrorMessage = ex.toString();
	    jsonOut = gson.toJson(mdlInquiryPassbookResult);
	    logger.error(LogAdapter.logControllerToLog4jException(startTime, 500, "/inquiry-passbook", "GET", customData, "", jsonOut, ex.toString()), ex);
	    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	}
	LogAdapter.InsertLog(mdlLog);
	return gson.toJson(mdlInquiryPassbookResult);
    }
}
