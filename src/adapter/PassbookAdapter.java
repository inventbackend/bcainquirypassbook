package adapter;

import java.text.MessageFormat;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class PassbookAdapter {
    final static Logger logger = LogManager.getLogger(PassbookAdapter.class);

    static Client client = Client.create();
    model.mdlLog mdlLog = new model.mdlLog();

    public static model.mdlPassbookResult GetPassbookData(String accountNumber, String serialNumber, String wsid) {
	long startTime = System.currentTimeMillis();
	String systemFunction = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlPassbookResult mdlPassbookResult = new model.mdlPassbookResult();
	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.ApiFunction = "getPassbookDetails";
	mdlLog.SystemFunction = systemFunction;
	mdlLog.LogSource = "Middleware";
	mdlLog.SerialNumber = serialNumber;
	mdlLog.WSID = wsid;
	mdlLog.LogStatus = "Success";
	mdlLog.ErrorMessage = "";

	String jsonIn, jsonOut = "";

	Gson gson = new Gson();
	String urlAPI = "/deposit-accounts/balance-details/" + accountNumber + "?user-id=" + wsid;

	Object[] params = new Object[] { accountNumber, serialNumber, wsid };
	jsonIn = MessageFormat.format("Account Number : {0}, SerialNumber : {1}, WSID : {2}", params);

	try {
	    // Get the base naming context from web.xml
	    Context context = (Context) new InitialContext().lookup("java:comp/env");

	    // Get a single value from web.xml
	    String MiddlewareIpAddress = (String) context.lookup("param_ipmiddleware");
	    String urlFinal = MiddlewareIpAddress + urlAPI;
	    String keyAPI = (String) context.lookup("param_keyAPI");
	    String ClientID = (String) context.lookup("param_clientid");
	    String decryptedClientID = EncryptAdapter.decrypt(ClientID, keyAPI);

	    // Client client = Client.create();
	    WebResource webResource = client.resource(urlFinal);
	    ClientResponse response = webResource.type("application/json").header("client-id", decryptedClientID).get(ClientResponse.class);
	    jsonOut = response.getEntity(String.class);
	    
	    logger.info(LogAdapter.logToLog4j(true, startTime, 200, urlAPI, "GET", jsonIn + ", function : " + systemFunction, "", jsonOut));

	    mdlPassbookResult = gson.fromJson(jsonOut, model.mdlPassbookResult.class);
	    if (mdlPassbookResult == null) {
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "API output is null";
	    } else if (!mdlPassbookResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = mdlPassbookResult.ErrorSchema.ErrorMessage.English;
	    }
	} catch (Exception ex) {
	    mdlPassbookResult = null;
	    mdlLog.LogStatus = "Failed";
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, urlAPI, "GET", jsonIn + ", function : " + systemFunction, "", jsonOut, ex.toString()), ex);
	    mdlLog.ErrorMessage = ex.toString();
	}
	LogAdapter.InsertLog(mdlLog);
	return mdlPassbookResult;
    }

    public static model.mdlAPIResult GetSmallPassbookStatement(String accountNumber, String serialNumber, String wsid) {
	long startTime = System.currentTimeMillis();
	String systemFunction = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlAPIResult mdlAPIResult = new model.mdlAPIResult();
	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.ApiFunction = "getPassbookDetails";
	mdlLog.SystemFunction = "GetPassbookData";
	mdlLog.LogSource = "Middleware";
	mdlLog.SerialNumber = serialNumber;
	mdlLog.WSID = wsid;
	mdlLog.LogStatus = "Success";
	mdlLog.ErrorMessage = "";

	Gson gson = new Gson();
	String urlAPI = "/passbooks/small-passbook/" + accountNumber;
	Object[] params = new Object[] { accountNumber, serialNumber, wsid };
	String jsonIn = MessageFormat.format("Account Number : {0}, SerialNumber : {1}, WSID : {2}", params);
	String jsonOut = "";

	try {
	    // Get the base naming context from web.xml
	    Context context = (Context) new InitialContext().lookup("java:comp/env");

	    // Get a single value from web.xml
	    String MiddlewareIpAddress = (String) context.lookup("param_ipmiddleware");
	    String urlFinal = MiddlewareIpAddress + urlAPI;
	    String keyAPI = (String) context.lookup("param_keyAPI");
	    String ClientID = (String) context.lookup("param_clientid");
	    String decryptedClientID = EncryptAdapter.decrypt(ClientID, keyAPI);

	    WebResource webResource = client.resource(urlFinal);
	    ClientResponse response = webResource.type("application/json").header("client-id", decryptedClientID).get(ClientResponse.class);
	    jsonOut = response.getEntity(String.class);
	    
	    logger.info(LogAdapter.logToLog4j(true, startTime, 200, urlAPI, "GET", jsonIn + ", function : " + systemFunction, "", jsonOut));

	    mdlAPIResult = gson.fromJson(jsonOut, model.mdlAPIResult.class);
	    if (mdlAPIResult == null) {
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "API output is null";
	    } else if (!mdlAPIResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = mdlAPIResult.ErrorSchema.ErrorMessage.English;
	    }
	} catch (Exception ex) {
	    mdlAPIResult = null;
	    mdlLog.LogStatus = "Failed";
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, urlAPI, "GET", jsonIn + ", function : " + systemFunction, "", jsonOut, ex.toString()), ex);
	    mdlLog.ErrorMessage = ex.toString();
	}
	LogAdapter.InsertLog(mdlLog);
	return mdlAPIResult;
    }
}
